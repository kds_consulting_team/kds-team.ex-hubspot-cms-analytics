# Analytics Breakdown Reports
from enum import Enum


class TimePeriod(Enum):
    total = 'total'  # - Data is rolled up to totals covering the specified time.
    daily = 'daily'  # - Grouped by day
    weekly = 'weekly'  # - Grouped by week
    monthly = 'monthly'  # - Grouped by month
    summarize_daily = 'summarize/daily'  # - Grouped by day, data is rolled up across all breakdowns
    summarize_weekly = 'summarize/weekly'  # - Grouped by week, data is rolled up across all breakdowns
    summarize_monthly = 'summarize/monthly'


class BreakdownBy(Enum):
    totals = 'totals'  # - Data is rolled up to totals covering the specified time.
    sessions = 'sessions'  # - Grouped by day
    sources = 'sources'  # - Grouped by week
    utm_campaigns = 'utm-campaigns'
    utm_contents = 'utm-contents'
    utm_mediums = 'utm-mediums'
    utm_sources = 'utm-sources'
    utm_terms = 'utm-terms'
    geolocation = 'geolocation'


class ObjectType(Enum):
    forms = 'forms'
    pages = 'pages'
    social_assists = 'social-assists'
    event_completions = 'event-completions'


class ContentType(Enum):
    landing_pages = 'landing-pages'  # - Pull data for landing pages.
    standard_pages = 'standard-pages'  # - Pull data for website pages.
    blog_posts = 'blog-posts'  # - Pull data for individual blog posts.
    listing_pages = 'listing-pages'  # - Pull data for blog listing pages.
    knowledge_articles = 'knowledge-articles'  # - Pull data for knowledge base articles.


class BreakdownTypesDailyNoFilter(Enum):
    """
        Combinations that do not require filters
    """
    totals_daily = {"type": BreakdownBy.totals, "time_period": TimePeriod.summarize_daily}
    sessions_daily = {"type": BreakdownBy.sessions, "time_period": TimePeriod.daily}
    sources_daily = {"type": BreakdownBy.sources, "time_period": TimePeriod.daily}
    utm_campaigns_daily = {"type": BreakdownBy.utm_campaigns, "time_period": TimePeriod.summarize_daily}
    utm_contents_daily = {"type": BreakdownBy.utm_contents, "time_period": TimePeriod.summarize_daily}
    utm_mediums_daily = {"type": BreakdownBy.utm_mediums, "time_period": TimePeriod.summarize_daily}
    utm_sources_daily = {"type": BreakdownBy.utm_sources, "time_period": TimePeriod.summarize_daily}
    utm_terms_daily = {"type": BreakdownBy.utm_terms, "time_period": TimePeriod.summarize_daily}
    geolocation_daily = {"type": BreakdownBy.geolocation, "time_period": TimePeriod.summarize_daily}


class BreakdownByObjectTypesDailyNoFilter(Enum):
    """
    Combinations that do not require filters
    """
    event_completions_daily = {"type": ObjectType.event_completions,
                               "time_period": TimePeriod.summarize_daily}
    forms_daily = {"type": ObjectType.forms, "time_period": TimePeriod.summarize_daily}
    pages_daily = {"type": ObjectType.pages, "time_period": TimePeriod.summarize_daily}
    social_assists_daily = {"type": ObjectType.social_assists, "time_period": TimePeriod.summarize_daily}


class BreakdownByContentTypesDailyNoFilter(Enum):
    """
    Combinations that do not require filters
    """
    landing_pages_daily = {"type": ContentType.landing_pages,
                           "time_period": TimePeriod.summarize_daily}
    blog_posts_daily = {"type": ContentType.blog_posts,
                        "time_period": TimePeriod.summarize_daily}
    knowledge_articles_daily = {"type": ContentType.knowledge_articles,
                                "time_period": TimePeriod.summarize_daily}
    listing_pages_daily = {"type": ContentType.listing_pages,
                           "time_period": TimePeriod.summarize_daily}
    standard_pages_daily = {"type": ContentType.standard_pages,
                            "time_period": TimePeriod.summarize_daily}
