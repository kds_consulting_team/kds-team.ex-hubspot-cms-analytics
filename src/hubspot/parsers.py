from typing import Iterator


def parse_analytics_breakdown(data: dict, breakdown_type: str = '') -> Iterator[dict]:
    for key, value in data.items():
        for csv_record in value:
            csv_record['date_period'] = key
            csv_record['content_type'] = breakdown_type
            yield csv_record


def parse_analytics_breakdown_total(period: str):
    def parser(data: dict, breakdown_type: str = '') -> Iterator[dict]:
        for csv_record in data['breakdowns']:
            csv_record['date_period'] = period
            csv_record['content_type'] = breakdown_type
            yield csv_record

    return parser
