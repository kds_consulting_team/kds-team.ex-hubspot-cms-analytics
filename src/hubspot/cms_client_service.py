import json
from typing import List, Union

from keboola.http_client import HttpClient

from hubspot.configuration_objects import BreakdownBy, \
    TimePeriod, ObjectType, ContentType

MAX_RETRIES = 10
BASE_URL = 'https://api.hubapi.com/'


class HubspotClientService(HttpClient):

    def __init__(self, token):
        HttpClient.__init__(self, base_url=BASE_URL, max_retries=MAX_RETRIES,
                            status_forcelist=(429, 500, 502, 504), default_params={"hapikey": token})

    def _get_paged_result_pages(self, endpoint, parameters, data, limit_attr='limit', offset_req_attr='offset',
                                offset_resp_attr='offset',
                                offset=0, limit=350):

        has_more = True
        while has_more:
            parameters[offset_req_attr] = offset
            parameters[limit_attr] = limit

            req = self.get_raw(self.base_url + endpoint, params=parameters, data=data)
            self._check_http_result(req, endpoint)
            resp_text = str.encode(req.text, 'utf-8')
            req_response = json.loads(resp_text)

            if req_response.get('total', 0) != 0:
                has_more = True
                offset = req_response[offset_resp_attr]
            else:
                has_more = False

            yield req_response

    def _check_http_result(self, response, endpoint):
        http_error_msg = ''
        if isinstance(response.reason, bytes):
            # We attempt to decode utf-8 first because some servers
            # choose to localize their reason strings. If the string
            # isn't utf-8, we fall back to iso-8859-1 for all other
            # encodings. (See PR #3538)
            try:
                reason = response.reason.decode('utf-8')
            except UnicodeDecodeError:
                reason = response.reason.decode('iso-8859-1')
        else:
            reason = response.reason
        if 401 == response.status_code:
            http_error_msg = u'Failed to login: %s - Please check your API token' % (reason)
        elif 401 < response.status_code < 500:
            http_error_msg = u'Request to %s failed %s Client Error: %s' % (endpoint, response.status_code, reason)

        elif 500 <= response.status_code < 600:
            http_error_msg = u'Request to %s failed %s Client Error: %s' % (endpoint, response.status_code, reason)

        if http_error_msg:
            raise RuntimeError(http_error_msg)

    def get_data_breakdowns_generic(self, breakdown_by: Union[ObjectType, BreakdownBy, ContentType],
                                    time_period: TimePeriod,
                                    start_date: str,
                                    end_date: str = None, filter_values: List[str] = None):

        endpoint = f'analytics/v2/reports/{breakdown_by.value}/{time_period.value}'
        parameters = {"start": start_date}
        if end_date:
            parameters['end'] = end_date

        # filter expression
        data = None
        if filter_values:
            data = {'f': filter_values}

        return self._get_paged_result_pages(endpoint, parameters=parameters, data=data, limit=1000)

    def get_data_breakdowns(self, breakdown_by: BreakdownBy, time_period: TimePeriod, start_date: str,
                            end_date: str = None, filter_values: List[str] = None):

        return self.get_data_breakdowns_generic(breakdown_by, time_period, start_date, end_date, filter_values)

    def get_data_breakdowns_by_object(self, object_type: ObjectType, time_period: TimePeriod, start_date: str,
                                      end_date: str = None, filter_values: List[str] = None):

        return self.get_data_breakdowns_generic(object_type, time_period, start_date, end_date, filter_values)

    def get_data_breakdowns_by_content_type(self, content_type: ContentType, time_period: TimePeriod, start_date: str,
                                            end_date: str = None, filter_values: List[str] = None):

        return self.get_data_breakdowns_generic(content_type, time_period, start_date, end_date, filter_values)
