'''
Template Component main class.

'''
import logging
import tempfile
from typing import List, Union, Tuple

import keboola.utils.date as kbdateutils
from keboola.component.base import ComponentBase
from keboola.component.dao import TableDefinition
from keboola.component.exceptions import UserException

# configuration variables
from configuration import Configuration, load_from_dict
from csv_tools import CachedOrthogonalDictWriter
from hubspot.cms_client_service import HubspotClientService
from hubspot.configuration_objects import BreakdownTypesDailyNoFilter, \
    TimePeriod, ContentType, ObjectType, BreakdownBy, BreakdownByObjectTypesDailyNoFilter, \
    BreakdownByContentTypesDailyNoFilter
from hubspot.parsers import parse_analytics_breakdown, parse_analytics_breakdown_total

KEY_API_TOKEN = '#api_token'
KEY_SINCE_DATE = 'date_since'
KEY_TO_DATE = 'date_to'
KEY_INCREMENTAL_OUTPUT = 'incremental_output'
KEY_LOADING_OPTIONS = 'loading_options'
# list of mandatory parameters => if some is missing,
# component will fail with readable message on initialization.
REQUIRED_PARAMETERS = [KEY_API_TOKEN, 'daily_breakdown']
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):
    """
        Extends base class for general Python components. Initializes the CommonInterface
        and performs configuration validation.

        For easier debugging the data folder is picked up by default from `../data` path,
        relative to working directory.

        If `debug` parameter is present in the `config.json`, the default logger is set to verbose DEBUG mode.
    """

    def __init__(self):
        super().__init__()

        self._client: HubspotClientService
        self._parameters: Configuration
        self._writer_cache = dict()

    def run(self):
        '''
        Main execution code
        '''

        # ####### EXAMPLE TO REMOVE
        # check for missing configuration parameters
        self.validate_configuration_parameters(REQUIRED_PARAMETERS)
        self._init_client()
        self._init_configuration_object()

        if self._parameters.daily_breakdown:
            results = self.get_breakdowns_totals_daily()
        else:
            results = self._download_analytics_breakdowns_summarized()

        self._close_writers()
        self.write_manifests(results)

    def _download_analytics_breakdowns_summarized(self):
        """
        Helper method to download and iterate through multiple breakdown types of summarized datasets.
        """
        if self._parameters.endpoint == 'analytics_data_breakdown':
            breakdown_types = self._parameters.data_breakdown_types_summarized
            breakdown_types_enum = BreakdownTypesDailyNoFilter

        elif self._parameters.endpoint == 'analytics_data_breakdown_by_object':
            breakdown_types = self._parameters.data_breakdown_objects_summarized
            breakdown_types_enum = BreakdownByObjectTypesDailyNoFilter

        elif self._parameters.endpoint == 'analytics_data_breakdown_by_content':
            breakdown_types = self._parameters.data_breakdown_content_summarized
            breakdown_types_enum = BreakdownByContentTypesDailyNoFilter

        else:
            raise UserException(f"The endpoint '{self._parameters.endpoint}' is not supported!")

        start_date = self._parameters.loading_options.date_since_formatted
        end_date = self._parameters.loading_options.date_to_formatted
        result_tables = []
        for breakdown_type in breakdown_types:
            breakdown_fields = breakdown_types_enum[breakdown_type].value
            object_type = breakdown_fields['type']
            time_period = breakdown_fields['time_period']

            results = self._download_analytics_breakdowns(breakdown_type, object_type,
                                                          start_date=start_date,
                                                          end_date=end_date,
                                                          time_period=time_period)
            result_tables.extend(results)

        return result_tables

    def get_breakdowns_totals_daily(self):
        start, end = kbdateutils.parse_datetime_interval(self._parameters.loading_options.date_since,
                                                         self._parameters.loading_options.date_to)

        date_chunks = kbdateutils.split_dates_to_chunks(start, end, 0, strformat='%Y%m%d')
        breakdown_name, breakdown_object = self._get_object_type()
        result_tables = set()
        for chunk in date_chunks:
            parser_method = parse_analytics_breakdown_total(chunk['start_date'])
            result_table = self._download_analytics_breakdowns(breakdown_type=breakdown_name,
                                                               object_type=breakdown_object,
                                                               start_date=chunk['start_date'],
                                                               end_date=chunk['end_date'],
                                                               time_period=TimePeriod.total,
                                                               parser_method=parser_method)
            result_tables.update(result_table)

        return result_tables

    def _download_analytics_breakdowns(self,
                                       breakdown_type: str,
                                       object_type: Union[ObjectType, BreakdownBy, ContentType],
                                       start_date: str,
                                       end_date: str,
                                       time_period=TimePeriod.daily,
                                       filter_list: List[str] = None,
                                       parser_method=parse_analytics_breakdown):

        incremental = self._parameters.loading_options.incremental_output
        result_tables = []

        primary_key = ['date_period', breakdown_type]
        if time_period == TimePeriod.daily or filter_list:
            primary_key.append('breakdown')

        out_table = self.create_out_table_definition(f'analytics_breakdown_by_{breakdown_type}.csv',
                                                     primary_key=primary_key,
                                                     incremental=incremental)

        writer = self._get_writer_from_cache(out_table)

        logging.info(f"Downloading {breakdown_type},"
                     f"in period {self._parameters.loading_options.date_since} "
                     f"- {self._parameters.loading_options.date_to}")

        results = self._client.get_data_breakdowns_generic(object_type,
                                                           time_period=time_period,
                                                           start_date=start_date,
                                                           end_date=end_date,
                                                           filter_values=filter_list)
        for result in results:
            writer.writerows(parser_method(result, breakdown_type))
        result_tables.append(out_table)

        return result_tables

    def _get_object_type(self) -> Tuple[str, Union[ObjectType, BreakdownBy, ContentType]]:
        if self._parameters.endpoint == 'analytics_data_breakdown':
            breakdown_name = self._parameters.break_down_type
            object_type = BreakdownBy[self._parameters.break_down_type]
        elif self._parameters.endpoint == 'analytics_data_breakdown_by_object':
            breakdown_name = self._parameters.break_down_object
            object_type = ObjectType[self._parameters.break_down_object]
        elif self._parameters.endpoint == 'analytics_data_breakdown_by_content':
            breakdown_name = self._parameters.break_down_content
            object_type = ContentType[self._parameters.break_down_content]
        else:
            raise UserException(f"The endpoint '{self._parameters.endpoint}' is not supported!")

        return breakdown_name, object_type

    def _get_writer_from_cache(self, out_table: TableDefinition):
        if not self._writer_cache.get(out_table.name):
            # init writer if not in cache
            self._writer_cache[out_table.name] = CachedOrthogonalDictWriter(out_table.full_path,
                                                                            out_table.primary_key.copy(),
                                                                            temp_directory=tempfile.mkdtemp())
            self._writer_cache[out_table.name].writeheader()

        return self._writer_cache[out_table.name]

    def _close_writers(self):
        for wr in self._writer_cache.values():
            wr.close()

    def _init_configuration_object(self):
        self._parameters: Configuration = load_from_dict(self.configuration.parameters)
        start, end = kbdateutils.parse_datetime_interval(self._parameters.loading_options.date_since,
                                                         self._parameters.loading_options.date_to, '%Y%m%d')
        # convert date format
        self._parameters.loading_options.date_since_formatted = start
        self._parameters.loading_options.date_to_formatted = end

    def _init_client(self):
        self._client = HubspotClientService(self.configuration.parameters[KEY_API_TOKEN])

    @property
    def client_method(self):
        mapping = {'analytics_data_breakdown': self._client.get_data_breakdowns,
                   'analytics_data_breakdown_by_object': self._client.get_data_breakdowns_by_object,
                   'analytics_data_breakdown_by_content': self._client.get_data_breakdowns_by_content_type}
        try:
            return mapping[self._parameters.endpoint]
        except KeyError:
            raise UserException(f"The endpoint '{self._parameters.endpoint}' is not supported!")


"""
        Main entrypoint
"""
if __name__ == "__main__":
    try:
        comp = Component()
        # this triggers the run method by default and is controlled by the configuration.action parameter
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
