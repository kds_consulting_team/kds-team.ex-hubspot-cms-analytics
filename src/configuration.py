import json
from dataclasses import dataclass
from typing import List

import dataconf


@dataclass
class LoadingOptions:
    date_to: str = 'now'
    date_since: str = '2005-01-01'
    date_to_formatted: str = ''
    date_since_formatted: str = ''
    incremental_output: bool = False


@dataclass
class Configuration:
    _api_token: str
    endpoint: str
    daily_breakdown: bool
    loading_options: LoadingOptions
    data_breakdown_types_summarized: List[str] = None
    break_down_type: str = ''
    data_breakdown_objects_summarized: List[str] = None
    break_down_object: str = ''
    data_breakdown_content_summarized: List[str] = None
    break_down_content: str = ''
    debug: bool = False


def load_from_dict(configuration: dict):
    json_conf = json.dumps(configuration)
    json_conf = json_conf.replace('#', '_')
    return dataconf.loads(json_conf, Configuration)
